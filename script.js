document.getElementById('submitButton').addEventListener('click', function() {

    let name = document.getElementById('name').value.trim();
    let email = document.getElementById('email').value.trim();
    let phone = document.getElementById('phone').value.trim();
    let address = document.getElementById('address').value.trim();
    let birthdate = document.getElementById('birthdate').value.trim();

    if (!name || !email || !phone || !address || !birthdate) {
        alert(" field!, Harap lengkapi semua.");
        return;
    }

    if (!validateEmail(email)) {
        alert("Email tidak valid!");
        return;
    }

    if (!validatePhone(phone)) {
        alert("Nomor telepon tidak valid!");
        return;
    }

    let shouldContinue = confirm("Apakah Anda ingin melanjutkan untuk menyimpan data?");
    if (shouldContinue) {
        let shouldSave = confirm("Apakah Anda ingin menyimpan data? Pilih 'OK' untuk menyimpan, atau 'Batal' untuk memperbaiki data.");
        if (shouldSave) {
            alert("Data berhasil disimpan!");
            alert(
                `Nama Lengkap: ${name}\n` +
                `Email: ${email}\n` +
                `Nomor Telepon: ${phone}\n` +
                `Alamat: ${address}\n` +
                `Tanggal Lahir: ${birthdate}`
            );
            document.getElementById('biodataForm').reset();
        } else {
            alert("Anda memilih untuk memperbaiki data.");
        }
    }

});



document.getElementById('resetButton').addEventListener('click', function() {

    let name = document.getElementById('name').value.trim();
    let email = document.getElementById('email').value.trim();
    let phone = document.getElementById('phone').value.trim();
    let address = document.getElementById('address').value.trim();
    let birthdate = document.getElementById('birthdate').value.trim();

    if (!name || !email || !phone || !address || !birthdate) {
        alert(" field!, Harap lengkapi semua.");
        return;
    }
    
    let shouldReset = confirm("Apakah Anda yakin ingin mereset formulir? Semua inputan akan dihapus.");
    if (shouldReset) {
        alert("Pengisian formulir direset ulang, silahkan isi kembali formulir.");
        document.getElementById('biodataForm').reset();
    }
});

function validateEmail(email) {
    const re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return re.test(String(email).toLowerCase());
}

function validatePhone(phone) {
    const re = /^\d{10,15}$/;
    return re.test(String(phone));
}

